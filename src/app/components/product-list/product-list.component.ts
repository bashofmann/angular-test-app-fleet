import { Component } from '@angular/core';

import { Product, products } from '../../products';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
})
export class ProductListComponent {
  products: Product[] = products;

  constructor(private _notificationService: NotificationsService) {}

  share() {
    this._notificationService.success('The product has been shared!');
  }

  onNotify() {
    this._notificationService.success(
      'You will be notified when the product goes on sale'
    );
  }
}
